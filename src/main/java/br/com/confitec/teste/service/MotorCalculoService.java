package br.com.confitec.teste.service;

import java.math.BigDecimal;

import br.com.confitec.teste.model.PlanoPagamento;


public interface MotorCalculoService{
	PlanoPagamento calcularParcelas(BigDecimal valor, Integer qtd, BigDecimal juros);
}
