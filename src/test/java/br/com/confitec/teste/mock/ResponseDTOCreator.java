package br.com.confitec.teste.mock;

import br.com.confitec.teste.model.PlanoPagamento;
import br.com.confitec.teste.model.dto.ResponseDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class ResponseDTOCreator {

    public static ResponseDTO create() {

        List<PlanoPagamento> planoPagamentoList = new ArrayList<>();
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(1).valorPrimeiraParcela(BigDecimal.valueOf(468.57))
                .valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(2).valorPrimeiraParcela(BigDecimal.valueOf(234.29))
                .valorDemaisParcelas(BigDecimal.valueOf(234.28)).valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(3).valorPrimeiraParcela(BigDecimal.valueOf(156.19))
                .valorDemaisParcelas(BigDecimal.valueOf(156.19)).valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(4).valorPrimeiraParcela(BigDecimal.valueOf(117.15))
                .valorDemaisParcelas(BigDecimal.valueOf(117.14)).valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(5).valorPrimeiraParcela(BigDecimal.valueOf(93.73))
                .valorDemaisParcelas(BigDecimal.valueOf(93.71)).valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(6).valorPrimeiraParcela(BigDecimal.valueOf(78.12))
                .valorDemaisParcelas(BigDecimal.valueOf(78.09)).valorParcelamentoTotal(BigDecimal.valueOf(468.57)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(7).valorPrimeiraParcela(BigDecimal.valueOf(71.81))
                .valorDemaisParcelas(BigDecimal.valueOf(71.76)).valorParcelamentoTotal(BigDecimal.valueOf(502.37)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(8).valorPrimeiraParcela(BigDecimal.valueOf(63.45))
                .valorDemaisParcelas(BigDecimal.valueOf(63.42)).valorParcelamentoTotal(BigDecimal.valueOf(507.39)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(9).valorPrimeiraParcela(BigDecimal.valueOf(56.95))
                .valorDemaisParcelas(BigDecimal.valueOf(56.94)).valorParcelamentoTotal(BigDecimal.valueOf(512.47)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(10).valorPrimeiraParcela(BigDecimal.valueOf(62.99))
                .valorDemaisParcelas(BigDecimal.valueOf(62.97)).valorParcelamentoTotal(BigDecimal.valueOf(629.72)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(11).valorPrimeiraParcela(BigDecimal.valueOf(59.01))
                .valorDemaisParcelas(BigDecimal.valueOf(58.96)).valorParcelamentoTotal(BigDecimal.valueOf(648.61)).build());
        planoPagamentoList.add(PlanoPagamento.builder().quantidadeParcelas(12).valorPrimeiraParcela(BigDecimal.valueOf(55.70).setScale(2))
                .valorDemaisParcelas(BigDecimal.valueOf(55.67)).valorParcelamentoTotal(BigDecimal.valueOf(668.07)).build());

        return ResponseDTO.builder().dados(planoPagamentoList).build();
    }
}
