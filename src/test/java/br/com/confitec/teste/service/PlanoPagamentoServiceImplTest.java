package br.com.confitec.teste.service;

import br.com.confitec.teste.model.dto.RequestDTO;
import br.com.confitec.teste.model.dto.ResponseDTO;
import br.com.confitec.teste.mock.RequestDTOCreator;
import br.com.confitec.teste.mock.ResponseDTOCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PlanoPagamentoServiceImplTest {

    @Autowired
    private PlanoPagamentoService planoPagamentoService;

    @Test
    public void testObterPlanosSucess() {

        RequestDTO request = RequestDTOCreator.create();
        ResponseDTO responseDTO = ResponseDTOCreator.create();

        ResponseDTO resultado = planoPagamentoService.obterPlanos(request);

        Assertions.assertEquals(responseDTO, resultado);
    }
    @Test
    public void testObterPlanosFailure() {
        RequestDTO request = RequestDTOCreator.createOnFailure();

        Assertions.assertThrows(NullPointerException.class, () -> {
            planoPagamentoService.obterPlanos(request);
        });
    }
}

