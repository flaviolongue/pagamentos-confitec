package br.com.confitec.teste.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.confitec.teste.model.OpcaoParcelamento;

public class QuantidateParcelaValidatorImpl implements ConstraintValidator<QuantidadeParcelaValidator, OpcaoParcelamento> {
	 public boolean isValid(OpcaoParcelamento opcao, ConstraintValidatorContext context) {
	        // Aqui você implementa a lógica da validação
	        if (opcao.getQuantidadeMaximaParcelas() < opcao.getQuantidadeMinimaParcelas()) {
	            return false;
	        }
	        return true;
	    }
}
