package br.com.confitec.teste.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlanoPagamento {

    private Integer quantidadeParcelas;
    private BigDecimal valorPrimeiraParcela;
    @JsonInclude(Include.NON_NULL)
    private BigDecimal valorDemaisParcelas;
    private BigDecimal valorParcelamentoTotal;

}
