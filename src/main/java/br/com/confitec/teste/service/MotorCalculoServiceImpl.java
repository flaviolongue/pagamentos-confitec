package br.com.confitec.teste.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import br.com.confitec.teste.model.PlanoPagamento;


@Service
public class MotorCalculoServiceImpl implements MotorCalculoService {
	public static final int PLANO_PARCELA_UNICA = 1;
	public static final int PRECISAO_CALCULO = 2;
	
	
	public PlanoPagamento calcularParcelas(BigDecimal valor, Integer qtdParcelas, BigDecimal juros) {
		BigDecimal valorTotal = valor.multiply((juros.add(BigDecimal.ONE).pow(qtdParcelas))).setScale(PRECISAO_CALCULO, RoundingMode.HALF_EVEN);
		if(qtdParcelas > PLANO_PARCELA_UNICA) {
			BigDecimal valorDemaisParcelas = valorTotal.divide(BigDecimal.valueOf(qtdParcelas), PRECISAO_CALCULO, RoundingMode.DOWN);
			BigDecimal valorPrimeiraParcela = valorDemaisParcelas.add(valorTotal.subtract(valorDemaisParcelas.multiply(BigDecimal.valueOf(qtdParcelas)))).setScale(PRECISAO_CALCULO, RoundingMode.UP);
			return PlanoPagamento.builder()
					.quantidadeParcelas(qtdParcelas)
					.valorParcelamentoTotal(valorTotal)
					.valorPrimeiraParcela(valorPrimeiraParcela)
					.valorDemaisParcelas(valorDemaisParcelas)
					.build();
		}else {
			return PlanoPagamento.builder()
					.quantidadeParcelas(qtdParcelas)
					.valorParcelamentoTotal(valorTotal)
					.valorPrimeiraParcela(valorTotal)
					.build();
		}
			
	}
}
