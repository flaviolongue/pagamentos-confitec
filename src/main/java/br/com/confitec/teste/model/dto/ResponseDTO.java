package br.com.confitec.teste.model.dto;

import br.com.confitec.teste.model.PlanoPagamento;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ResponseDTO {

    private List<PlanoPagamento> dados;

}
