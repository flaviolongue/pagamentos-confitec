package br.com.confitec.teste.model;

import java.math.BigDecimal;

import javax.validation.constraints.Min;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cobertura {
	
    private Integer cobertura;
    @Min(value = 0, message = "Somente valores maior igual a zero")
    private BigDecimal valor;

}
