package br.com.confitec.teste.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.confitec.teste.model.dto.RequestDTO;
import br.com.confitec.teste.model.dto.ResponseDTO;
import br.com.confitec.teste.service.PlanoPagamentoService;

@Validated
@RestController
@RequestMapping("/confitec/teste")
public class ParcelamentoController {

    @Autowired
    private PlanoPagamentoService planoPagamentoService;

    @PostMapping("/parcelamento")
    public ResponseEntity<ResponseDTO> calcularPlanoPagamento(@RequestBody @Valid RequestDTO request) {
        ResponseDTO resultado = planoPagamentoService.obterPlanos(request);
        return ResponseEntity.ok(resultado);
    }
}
