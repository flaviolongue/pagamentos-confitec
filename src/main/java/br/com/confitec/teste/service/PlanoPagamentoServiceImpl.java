package br.com.confitec.teste.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.confitec.teste.model.PlanoPagamento;
import br.com.confitec.teste.model.dto.RequestDTO;
import br.com.confitec.teste.model.dto.ResponseDTO;

@Service
public class PlanoPagamentoServiceImpl implements PlanoPagamentoService {

@Autowired
private MotorCalculoService calculador;	
	
    public ResponseDTO obterPlanos(RequestDTO request) {
    	
    	BigDecimal valorTotalCoberturas = request.getListCobertura().stream().map(x -> x.getValor()).reduce(BigDecimal.ZERO, BigDecimal::add);
    	List<PlanoPagamento> planoPagamentoList = new ArrayList<>();
    	request.getListOpcaoParcelamento().forEach(p-> {  
    		planoPagamentoList.addAll(calculaOpcaoParcelamento(valorTotalCoberturas, p.getQuantidadeMinimaParcelas(), p.getQuantidadeMaximaParcelas(), p.getJuros()));
    	});
    	return ResponseDTO.builder()
                .dados(planoPagamentoList)
                .build();
    }
    
    private List<PlanoPagamento> calculaOpcaoParcelamento(BigDecimal valorTotalCobertura, Integer min, Integer max, BigDecimal juros) {
		 List<PlanoPagamento> planoPagamentoList = new ArrayList<>();
		 for (int i = min; i <= max; i++) {
			 planoPagamentoList.add(calculador.calcularParcelas(valorTotalCobertura,  i,  juros));
		 }
	     
		return planoPagamentoList;
	}
  
}
