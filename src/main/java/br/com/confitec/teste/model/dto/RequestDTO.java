package br.com.confitec.teste.model.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.confitec.teste.model.Cobertura;
import br.com.confitec.teste.model.OpcaoParcelamento;
import br.com.confitec.teste.validators.QuantidadeParcelaValidator;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequestDTO {
	@Valid
	@NotNull(message = "Não pode vir null")
    private List<Cobertura> listCobertura;
	@NotNull(message = "Não pode vir null")
	@Valid
    private List<OpcaoParcelamento> listOpcaoParcelamento;

}
