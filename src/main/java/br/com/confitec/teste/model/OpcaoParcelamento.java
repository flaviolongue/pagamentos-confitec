package br.com.confitec.teste.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

import javax.validation.constraints.Min;

import org.springframework.validation.annotation.Validated;

import br.com.confitec.teste.validators.QuantidadeParcelaValidator;

@Data
@Builder
@Validated
@QuantidadeParcelaValidator
public class OpcaoParcelamento {
	
	@Min(value = 0, message = "Somente valores maior igual a zero")
    private Integer quantidadeMinimaParcelas;
    private Integer quantidadeMaximaParcelas;
    private BigDecimal juros;

}
