package br.com.confitec.teste.service;

import br.com.confitec.teste.model.dto.RequestDTO;
import br.com.confitec.teste.model.dto.ResponseDTO;


public interface PlanoPagamentoService {
	ResponseDTO obterPlanos(RequestDTO request);
}
