package br.com.confitec.teste.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;


@Target({ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = QuantidateParcelaValidatorImpl.class)
public @interface QuantidadeParcelaValidator {
	 String message() default "MAXIMO DE PARCELA NÃO PODE SER MENOR QUE O MINIMO";
	 Class<?>[] groups() default {};
	 Class<? extends javax.validation.Payload>[] payload() default {};
}
