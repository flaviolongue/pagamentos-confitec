package br.com.confitec.teste.mock;

import br.com.confitec.teste.model.Cobertura;
import br.com.confitec.teste.model.OpcaoParcelamento;
import br.com.confitec.teste.model.dto.RequestDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RequestDTOCreator {

    public static RequestDTO create() {

        List<Cobertura> coberturaList = new ArrayList<>();
        coberturaList.add(Cobertura.builder().cobertura(1).valor(BigDecimal.valueOf(123.12)).build());
        coberturaList.add(Cobertura.builder().cobertura(4).valor(BigDecimal.valueOf(345.45)).build());

        List<OpcaoParcelamento> opcaoParcelamentoList = new ArrayList<>();
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(1).quantidadeMaximaParcelas(6).juros(BigDecimal.valueOf(0)).build());
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(7).quantidadeMaximaParcelas(9).juros(BigDecimal.valueOf(0.01)).build());
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(10).quantidadeMaximaParcelas(12).juros(BigDecimal.valueOf(0.03)).build());

        return RequestDTO.builder()
               .listCobertura(coberturaList)
               .listOpcaoParcelamento(opcaoParcelamentoList)
               .build();
    }

    public static RequestDTO createOnFailure() {

        List<Cobertura> coberturaList = new ArrayList<>();
        coberturaList.add(Cobertura.builder().cobertura(null).valor(null).build());

        List<OpcaoParcelamento> opcaoParcelamentoList = new ArrayList<>();
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(null).quantidadeMaximaParcelas(null).juros(null).build());

        return RequestDTO.builder()
                .listCobertura(coberturaList)
                .listOpcaoParcelamento(opcaoParcelamentoList)
                .build();
    }

    public static RequestDTO createOnFailureWithValueNegative() {

        List<Cobertura> coberturaList = new ArrayList<>();
        coberturaList.add(Cobertura.builder().cobertura(1).valor(BigDecimal.valueOf(-123.12)).build());

        List<OpcaoParcelamento> opcaoParcelamentoList = new ArrayList<>();
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(1).quantidadeMaximaParcelas(6).juros(BigDecimal.valueOf(0)).build());

        return RequestDTO.builder()
                .listCobertura(coberturaList)
                .listOpcaoParcelamento(opcaoParcelamentoList)
                .build();
    }

    public static RequestDTO createOnFailureWithQtdParcelamento() {

        List<Cobertura> coberturaList = new ArrayList<>();
        coberturaList.add(Cobertura.builder().cobertura(1).valor(BigDecimal.valueOf(123.12)).build());

        List<OpcaoParcelamento> opcaoParcelamentoList = new ArrayList<>();
        opcaoParcelamentoList.add(OpcaoParcelamento.builder().quantidadeMinimaParcelas(6).quantidadeMaximaParcelas(1).juros(BigDecimal.valueOf(0)).build());

        return RequestDTO.builder()
                .listCobertura(coberturaList)
                .listOpcaoParcelamento(opcaoParcelamentoList)
                .build();
    }
}