package br.com.confitec.teste.controller;

import br.com.confitec.teste.mock.RequestDTOCreator;
import br.com.confitec.teste.mock.ResponseDTOCreator;
import br.com.confitec.teste.model.dto.RequestDTO;
import br.com.confitec.teste.model.dto.ResponseDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import javax.validation.ConstraintViolationException;

@SpringBootTest
public class ParcelamentoControllerTest {

    @Autowired
    private ParcelamentoController parcelamentoController;

    @Test
    public void testCalcularPlanoPagamentoSucess() {

        RequestDTO request = RequestDTOCreator.create();
        ResponseDTO responseDTO = ResponseDTOCreator.create();

        ResponseEntity<ResponseDTO> resultado = parcelamentoController.calcularPlanoPagamento(request);

        Assertions.assertEquals(responseDTO, resultado.getBody());
    }

    @Test
    public void testCalcularPlanoPagamentoFailureWithValueNegative() {
        RequestDTO request = RequestDTOCreator.createOnFailureWithValueNegative();

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            parcelamentoController.calcularPlanoPagamento(request);
        });
    }

    @Test
    public void testCalcularPlanoPagamentoFailureWithQtdParcelamento() {
        RequestDTO request = RequestDTOCreator.createOnFailureWithQtdParcelamento();

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            parcelamentoController.calcularPlanoPagamento(request);
        });
    }
}

